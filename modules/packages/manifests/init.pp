# Class: packages
#
# Parameters: none
#
# Actions: Declara recursos virtuais
#
# Sample Usage:
#
#   include packages
#
class packages{
  
  #Declaracao de recurso virtual para listar pacotes que podem ser instalados
  @package { ['gcc', 
             'gcc-c++',
             'g++',
             'dialog',
             'sshpass',
             'rssh',
             'subversion',
             'gawk',
             'gzip',
             'cpp',
            ]:
    ensure => installed,
  }
}
