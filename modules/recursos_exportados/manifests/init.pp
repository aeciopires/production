# Class: recursos_exportados
#
# Parameters: none
#
# Actions: Declara a exportacao de alguns recursos
#
# Sample Usage:
#
#   include recursos_exportados
#
class recursos_exportados{
  
  #Declaracao de recurso exportado
  @@file { "/tmp/${::hostname}_sshrsakey.txt":
    ensure  => file,
    #content => "$::sshdsakey root@${::hostname}",
    content => "$::sshrsakey",
    mode    => '700',
    owner   => 'root',
    group   => 'root',
  }
}
