# Class: install_mytools
#
#
# Parameters: none
#
# Actions: Faz uso de recursos virtuais e coleta recursos exportados para instalar pacotes e gerenciar o conteudo de arquivos
#
# Requires: Requer o uso dos modulos packages e recursos_exportados
#
# Sample Usage:
#
#   include install_mytools
#
class install_mytools{
    
  #Incluindo as classes de outros modulos requisitos
  include packages
  include recursos_exportados
  #--------------------------------------------------------------------#

  #Instalando pacotes requisitos atraves do uso dos recursos virtuais
  realize(Package['subversion'],
          Package['dialog'],
          Package['sshpass'],
          Package['rssh'],
          Package['gzip'],
         )

  #Coletando todos os recursos exportados do tipo 'File'
  File <<| |>>
}
